package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn){
        HashMap<String, Double> result = new HashMap<>();
        result.put("1",10.0);
        result.put("2",45.0);
        result.put("3",20.0);
        result.put("4",35.0);
        result.put("5",50.0);

        return result.getOrDefault(isbn, 0.0);
    }
}
